# `getanthill` Security Layer

[![pipeline](https://gitlab.com/getanthill/sec/badges/master/pipeline.svg)](https://gitlab.com/getanthill/sec/-/commits/master) [![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=getanthill%2Fsec&metric=alert_status)](https://sonarcloud.io/dashboard?id=getanthill%2Fsec)

[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=getanthill%2Fsec&metric=coverage)](https://sonarcloud.io/dashboard?id=getanthill%2Fsec) [![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=getanthill%2Fsec&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=getanthill%2Fsec) [![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=getanthill%2Fsec&metric=security_rating)](https://sonarcloud.io/dashboard?id=getanthill%2Fsec)

## Getting Started

First, you need install the package:

```shell
npm install -S @getanthill/sec
```

Then, your client can follow the steps to register or signin a
user to your app, then activate MFA validation.

```typescript
// 1. Signin part:
const tokens = auth.getTokens(email, password);

// 2. MFA Setup part: (only on registration)
// Master key generation #keep-it-secret-keep-it-safe
const masterKey = otp.generateMasterKey();
// Secret key generation from Master key
const secretKey = otp.generateSecretFromMaster(masterKey);

// Validate MFA:
const isValid = otp.verify('123456', secretKey);

// Create the store:
const store = new Store(tokens.encryption, secretKey);

// Manage your data as always:
store.data = { hello: 'world' }; // Always encrypted data
```

## Roadmap

- [x] Implements the Data Privacy Security Layer by default described here:
      https://hacks.mozilla.org/2018/11/firefox-sync-privacy/
- [x] Implements TOTP
- [ ] Implements the QRCode generation part
