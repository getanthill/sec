import { Blob as BlobPolyfill } from 'node:buffer';
import { CompressionStream, DecompressionStream } from 'node:stream/web';
import { TextEncoder, TextDecoder } from 'node:util';

import { Crypto } from '@peculiar/webcrypto';

export default function setup() {
  console.log('Global setup');
}

setup.init = function init() {
  Object.defineProperty(global, 'crypto', {
    value: new Crypto(),
  });
  Object.defineProperty(global, 'Blob', {
    value: BlobPolyfill,
  });
  Object.defineProperty(global, 'CompressionStream', {
    value: CompressionStream,
  });
  Object.defineProperty(global, 'DecompressionStream', {
    value: DecompressionStream,
  });
  Object.assign(global, { TextDecoder, TextEncoder });
};
