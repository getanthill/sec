import * as sec from '../../src';

describe('integration/signup flow', () => {
  beforeEach(() => {
    Date.now = jest.fn().mockReturnValue(0);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('allows a user to register', () => {
    const email = 'john@doe.com';
    const password = 'MyPassword!';

    // Retrieving token from email and password:
    const tokens = sec.getTokens(email, password);

    expect(tokens).toHaveProperty('auth');
    expect(tokens).toHaveProperty('encryption');

    // Generation of a Master Key for 2FA authentication:
    const master = 'L4ZB2DCRNJ4CAFBINEPBIZI6N4';
    const secret = sec.generateSecretFromMaster(master);

    // Generation of a One-Time Password:
    // > fixed because of the stub on Data.now
    const token = sec.totp(secret);
    expect(token).toEqual('154976');

    // Instanciation of the Store with the encryption token
    // and the MFA secret
    const store = new sec.Store(tokens.encryption, secret);

    store.data = { hello: 'world' };

    const store2 = new sec.Store(tokens.encryption, secret, store.encryptedData);
    expect(store2.data).toEqual(store.data);
  });

  it('allows a user to signin and access his stored data', () => {
    const storedData =
      '{"iv":"taHTYgSMqlJhWYb741U+qQ==","v":1,"iter":10000,"ks":128,"ts":64,"mode":"ccm","adata":"","cipher":"aes","salt":"3ddFrvs99Z8=","ct":"wzNtglE3eum3t+46nwj8Vm1Rvw0uLa5oJg=="}';

    // Signin action:
    const tokens = sec.getTokens('john@doe.com', 'MyPassword!');

    // 2FA validation of the secret:
    const secret = 'DUNXSLAMLUPGABRAGVGAM7JCDNYS452ANIED6CKAJ4NWONCOHIMA';

    // Loading of the store with already stored data:
    const store = new sec.Store(tokens.encryption, secret, storedData);

    expect(store.data).toEqual({ hello: 'world' });
  });
});
