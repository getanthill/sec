export interface Tokens {
  auth: string;
  encryption: string;
  [key: string]: string;
}

export interface AuthGetTokensOptions {
  tokens?: string[];
  namespace?: string;
}
