# [0.7.0](https://gitlab.com/getanthill/sec/compare/v0.6.0...v0.7.0) (2024-01-15)


### Bug Fixes

* **build:** fix node version in gitlab ([e88eeeb](https://gitlab.com/getanthill/sec/commit/e88eeebca28602a22b2c15898fd279a8e5582930))


### Features

* **data:** allows to encrypt large data with RSA keys ([ac820b6](https://gitlab.com/getanthill/sec/commit/ac820b642546346c4be2518a5046fb9f1c7d9823))

# [0.6.0](https://gitlab.com/getanthill/sec/compare/v0.5.0...v0.6.0) (2024-01-11)


### Features

* **e2e:** implementation of e2e logic ([128eb09](https://gitlab.com/getanthill/sec/commit/128eb0901b3a2ba2cd9037ff6a91f4863e21d32a))

# [0.5.0](https://gitlab.com/getanthill/sec/compare/v0.4.0...v0.5.0) (2022-11-22)


### Features

* upgrade Node.js to 18 ([89bfc25](https://gitlab.com/getanthill/sec/commit/89bfc25e13c3f7a9535c99f2b2f360c35bb2f108))

# [0.4.0](https://gitlab.com/getanthill/sec/compare/v0.3.4...v0.4.0) (2022-05-21)


### Features

* end-to-end encryption ([12cf759](https://gitlab.com/getanthill/sec/commit/12cf759fb5cca538d34d110bcd683ac70b439bb2))
