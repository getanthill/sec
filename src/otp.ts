import { authenticatorEncoder } from '@otplib/core';
import * as otplib from 'otplib';

import * as crypto from './crypto';

export function generateMasterKey(): string {
  const options = otplib.authenticator.allOptions();
  return authenticatorEncoder(
    options.createRandomBytes(16, options.encoding),
    options,
  ) as string;
}

export function generateSecretFromMaster(masterKey: string): string {
  const s = crypto.hexFromBits(crypto.pbkdf2(masterKey, ''));

  return authenticatorEncoder(s, otplib.authenticator.allOptions()) as string;
}

export function totp(secret, options = {}) {
  fixUint8ArraySet();
  otplib.authenticator.options = {
    ...otplib.authenticator.options,
    ...options,
  };
  const otp = otplib.authenticator.generate(secret);

  restoreUint8ArraySet();
  otplib.authenticator.resetOptions();

  return otp;
}

export function verify(token, secret, options = {}) {
  return token === totp(secret, options);
}

/**
 * Generate the Google Authenticator
 * @param secret
 * @param label
 * @param issuer
 * @param type
 */
export function getAuthenticatorUri(
  secret,
  user = 'me',
  service = 'getanthill',
  options = {},
) {
  otplib.authenticator.options = {
    ...otplib.authenticator.options,
    ...options,
  };

  const uri = otplib.authenticator.keyuri(user, service, secret);

  otplib.authenticator.resetOptions();

  return uri;
}

/* istanbul ignore next */
let restoreUint8ArraySet = () => null;
/* istanbul ignore next */
let fixUint8ArraySet = () => null;

// Polyfill and fix browser issues with otplib...
/* istanbul ignore next */
if (typeof window !== 'undefined') {
  // (window as any).global = window;
  // global.Buffer = global.Buffer || require('buffer').Buffer;

  /**
   * WTF Uint8Array.prototype.set issue in the browser
   * offset is out of bound...
   */
  const Uint8ArraySet = Uint8Array.prototype.set;
  restoreUint8ArraySet = () => {
    Uint8Array.prototype.set = Uint8ArraySet;
  };

  fixUint8ArraySet = () => {
    // const Uint8ArraySet = Uint8Array.prototype.set;
    Uint8Array.prototype.set = function (
      array: ArrayLike<number>,
      offset?: number,
    ): void {
      Uint8ArraySet.apply(this, [
        // @ts-ignore
        array.slice(0, Math.min(this.length, array.length) - offset),
        offset,
      ]);
    };
  };
}
