import { Store } from './store';

describe('encryption', () => {
  describe('constructor', () => {
    it('returns a valid constructor', () => {
      const store = new Store('encryption', 'secret');

      expect(store).toBeInstanceOf(Store);
      expect(store).toHaveProperty('encryptionToken', 'encryption');
      expect(store).toHaveProperty('mfaSecret', 'secret');
    });

    it('supports non-configured MFA stores', () => {
      const store = new Store('encryption');

      expect(store).toBeInstanceOf(Store);
      expect(store).toHaveProperty('encryptionToken', 'encryption');
      expect(store).toHaveProperty('mfaSecret', null);

      store.data = { hello: 'world' };
      expect(store.data).toEqual({ hello: 'world' });
    });
  });

  describe('getter/setter data', () => {
    const store = new Store('encryption', 'secret');
    it('stores encrypted values only', () => {
      store.data = { hello: 'world' };

      // @ts-ignore
      expect(JSON.parse(store.encryptedData)).toMatchObject({
        v: 1,
        iter: 10000,
        ks: 128,
        ts: 64,
        mode: 'ccm',
        adata: '',
        cipher: 'aes',
      });

      expect(store.data).toEqual({ hello: 'world' });
    });

    it('allows another instance to decrypt with the same encryption token and MFA secret', () => {
      store.data = { hello: 'world' };

      const storeB = new Store('encryption', 'secret', store.encryptedData);
      expect(storeB.data).toEqual({ hello: 'world' });
    });
  });

  describe('#set', () => {
    let store;
    beforeEach(() => {
      store = new Store('encryption', 'secret');
    });

    it('allows to set a specific key only', () => {
      store.set('hello', 'world');

      expect(store.data).toEqual({ hello: 'world' });
      expect(store.get('hello')).toEqual('world');
    });
  });

  describe('#get', () => {
    let store;
    beforeEach(() => {
      store = new Store('encryption', 'secret');
    });

    it('allows to get a specific key from the encrypted data', () => {
      store.set('hello', 'world');

      expect(store.get('hello')).toEqual('world');
    });

    it('returns default value if not found in the current store', () => {
      expect(store.get('hello', 'world!!')).toEqual('world!!');
    });
  });
});
