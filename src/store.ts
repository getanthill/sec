import * as sjcl from 'sjcl-including-ecc';
export class Store {
  encryptionToken: string;
  mfaSecret: string;
  encryptedData: sjcl.SjclCipherEncrypted;

  constructor(
    encryptionToken: string,
    mfaSecret: string = null,
    encryptedData: sjcl.SjclCipherEncrypted = null,
  ) {
    this.encryptionToken = encryptionToken;
    this.mfaSecret = mfaSecret;

    if (encryptedData) {
      this.encryptedData = encryptedData;
    } else {
      this.data = {};
    }
  }

  getEncryptionKey(
    encryptionToken = this.encryptionToken,
    mfaSecret = this.mfaSecret,
  ) {
    return `${encryptionToken}:${mfaSecret || ''}`;
  }

  get data(): object {
    return this.deserialize(this.decrypt(this.encryptedData));
  }

  set data(clearData) {
    this.encryptedData = this.encrypt(this.serialize(clearData));
  }

  set(key: string, value: any): void {
    this.data = {
      ...this.data,
      [key]: value,
    };
  }

  get(key: string, defaultValue: any): any {
    return this.data[key] || defaultValue;
  }

  serialize(data: object): string {
    return JSON.stringify(data);
  }

  deserialize(data: string): sjcl.SjclCipherEncrypted {
    return JSON.parse(data);
  }

  encrypt(clearData: string): sjcl.SjclCipherEncrypted {
    return sjcl.encrypt(this.getEncryptionKey(), clearData);
  }

  decrypt(encryptedData: sjcl.SjclCipherEncrypted): string {
    return sjcl.decrypt(this.getEncryptionKey(), encryptedData);
  }
}
