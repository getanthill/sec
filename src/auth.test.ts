import * as auth from './auth';

describe('auth', () => {
  it('must provides an authentication token from email and password', () => {
    const res = auth.getTokens('john@doe.com', 'HelloWorld!');

    expect(res).toHaveProperty('auth');
  });

  it('must provides an encryption token from email and password', () => {
    const res = auth.getTokens('john@doe.com', 'HelloWorld!');

    expect(res).toHaveProperty('encryption');
  });

  it('must provides different keys between auth and encryption tokens', () => {
    const res = auth.getTokens('john@doe.com', 'HelloWorld!');

    expect(res.auth).not.toEqual(res.encryption);
  });

  it('must generates the same token from the same email and password', () => {
    const res1 = auth.getTokens('john@doe.com', 'HelloWorld!');
    const res2 = auth.getTokens('john@doe.com', 'HelloWorld!');

    expect(res1).toEqual(res2);
  });

  it('must generates additional tokens per usage', () => {
    const res0 = auth.getTokens('john@doe.com', 'HelloWorld!');
    const res = auth.getTokens('john@doe.com', 'HelloWorld!', {
      tokens: ['com'],
    });

    expect(res).toMatchObject(res0);
    expect(res).toHaveProperty('com');
    expect(res.com).toEqual(
      'b510448187dbd46facf627c9dd593dc92b1654163bc106bd72577b91cbfe7dea',
    );
  });

  it('allow migration between namespaces', () => {
    const tokens0 = auth.getTokens('john@doe.com', 'HelloWorld!', {
      namespace: 'getanthill.com/sec/v1/',
    });
    const tokens1 = auth.getTokens('john@doe.com', 'HelloWorld!', {
      namespace: 'getanthill.com/sec/v2/',
    });

    expect(tokens0).not.toMatchObject(tokens1);
  });
});
