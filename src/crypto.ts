import * as sjcl from 'sjcl-including-ecc';

// Key wrapping and stretching configuration.
const NAMESPACE: string = 'getanthill.com/sec/v1/';
const PBKDF2_ROUNDS: number = 1000;
const PBKDF2_STRETCHED_PASS_LENGTH_BYTES: number = 32 * 8;

const HKDF_SALT: number[] = sjcl.codec.hex.toBits('00');
const HKDF_LENGTH: number = 32;

const KWE_SEPARATOR: string = ':';

export function stringToBits(str: string): number[] {
  return sjcl.codec.utf8String.toBits(str);
}

export function hexFromBits(bits: number[]): string {
  return sjcl.codec.hex.fromBits(bits);
}

export function pbkdf2(input, salt) {
  return sjcl.misc.pbkdf2(
    input,
    salt,
    PBKDF2_ROUNDS,
    PBKDF2_STRETCHED_PASS_LENGTH_BYTES,
    sjcl.misc.hmac,
  );
}

/**
 * Key wrapping with a name
 * @param name Name to be wrapped
 * @param namespace Namespace to use to wrap keys
 * @returns {bitArray}
 */
export function kw(name: string, namespace: string = NAMESPACE): number[] {
  return stringToBits(namespace + name);
}

/**
 * Key Wrapping with a name and an email
 *
 * @method kwe
 * @static
 * @param {String} name The name of the salt.
 * @param {String} email The email of the user.
 * @param {String} namespace The namespace to use.
 * @return {bitArray} the salt combination with the namespace
 */
export function kwe(name: string, email: string, namespace?: string): number[] {
  return kw(name + KWE_SEPARATOR + email, namespace);
}

/**
 * hkdf - The HMAC-based Key Derivation Function
 * based on https://github.com/mozilla/node-hkdf
 *
 * @class hkdf
 * @param {bitArray} ikm Initial keying material
 * @param {bitArray} info Key derivation data
 * @param {bitArray} [salt=HKDF_SALT] Salt
 * @param {integer} [length=HKDF_LENGTH] Length of the derived key in bytes
 * @return promise object- It will resolve with `output` data
 */
export function hkdf(ikm, info, salt = HKDF_SALT, length = HKDF_LENGTH) {
  const mac = new sjcl.misc.hmac(salt, sjcl.hash.sha256);
  mac.update(ikm);

  // compute the PRK
  const prk = mac.digest();

  // hash length is 32 because only sjcl.hash.sha256 is used at this moment
  const hashLength = 32;
  const num_blocks = Math.ceil(length / hashLength);
  let prev = sjcl.codec.hex.toBits('');
  let output = '';

  for (let i = 0; i < num_blocks; i++) {
    const hmac = new sjcl.misc.hmac(prk, sjcl.hash.sha256);

    const input = sjcl.bitArray.concat(
      sjcl.bitArray.concat(prev, info),
      sjcl.codec.utf8String.toBits(String.fromCharCode(i + 1)),
    );

    hmac.update(input);

    prev = hmac.digest();
    output += sjcl.codec.hex.fromBits(prev);
  }

  const truncated = sjcl.bitArray.clamp(
    sjcl.codec.hex.toBits(output),
    length * 8,
  );

  return truncated;
}

export function generateKeys() {
  const keys = sjcl.ecc.elGamal.generateKeys(384, 1); //choose a stronger/weaker curve

  const pubkem = keys.pub.kem(); //KEM is Key Encapsulation Mechanism
  const publicKey = pubkem.key;
  const privateKey = keys.sec.unkem(pubkem.tag); //tag is used to derive the secret (private) key

  return {
    public: publicKey,
    private: privateKey,
  };
}

export function encrypt(publicKey: string, content: string) {
  return sjcl.encrypt(publicKey, content);
}

export function decrypt(privateKey: string, encryptedContent) {
  return sjcl.decrypt(privateKey, encryptedContent);
}

export function encryptJSON(publicKey: string, content: any) {
  return encrypt(publicKey, JSON.stringify(content));
}

export function decryptJSON(privateKey: string, encryptedContent) {
  return JSON.parse(decrypt(privateKey, encryptedContent));
}
