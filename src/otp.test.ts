import * as otp from './otp';

describe('otp', () => {
  const secret = 'BTIDA42P7Z4P6C3O';
  beforeEach(() => {
    Date.now = jest.fn().mockReturnValue(0);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('#generateMasterKKey', () => {
    it('returns a long secret key for OTP secret generation', () => {
      const masterKey = otp.generateMasterKey();
      expect(masterKey.length).toEqual(26);
    });
  });

  describe('#totp - Time-based One-Time Password', () => {
    it('provides a Time-based One-Time Password', () => {
      const password1 = otp.totp(secret);
      const password2 = otp.totp(secret);

      expect(password1).toEqual(password2);
    });

    it('provides a totp at the specified length', () => {
      const password1 = otp.totp(secret, { digits: 4 });

      expect(password1).toEqual('2862');
    });

    it('provides a totp for the specified time window', () => {
      Date.now = jest.fn().mockReturnValueOnce(0).mockReturnValue(30000);

      const password1 = otp.totp(secret, {
        step: 20,
      });
      const password2 = otp.totp(secret, {
        step: 20,
      });

      expect(password1).toEqual('292862');
      expect(password2).toEqual('338222');

      expect(password1).not.toEqual(password2);
    });

    it('provides the same One-Time Password within the same time window', () => {
      Date.now = jest.fn().mockReturnValueOnce(0).mockReturnValue(10000);

      const password1 = otp.totp(secret, {
        step: 30,
      });
      const password2 = otp.totp(secret, {
        step: 30,
      });

      expect(password1).toEqual('292862');
      expect(password2).toEqual('292862');

      expect(password1).toEqual(password2);
    });

    it('provides a One-Time Password per time window', () => {
      Date.now = jest.fn().mockReturnValueOnce(0).mockReturnValue(30000);

      const password1 = otp.totp(secret);
      const password2 = otp.totp(secret);

      expect(password1).toEqual('292862');
      expect(password2).toEqual('338222');

      expect(password1).not.toEqual(password2);
    });
  });

  describe('#verify', () => {
    it('returns true if the OTP is valid', () => {
      Date.now = jest.fn().mockReturnValue(30000);

      const totp = otp.totp(secret);

      expect(otp.verify(totp, secret)).toEqual(true);
    });

    it('returns false if the OTP expired', () => {
      Date.now = jest.fn().mockReturnValueOnce(0).mockReturnValue(30000);

      const totp = otp.totp(secret);

      expect(otp.verify(totp, secret)).toEqual(false);
    });

    it('returns false if a wrong secret is used', () => {
      Date.now = jest.fn().mockReturnValue(30000);

      const totp = otp.totp('bad');

      expect(otp.verify(totp, 'good')).toEqual(false);
    });
  });

  describe('#getAuthenticatorUri', () => {
    it('returns a valid Google Authenticator URI', () => {
      const secretFromMaster = otp.generateSecretFromMaster('master');

      const uri = otp.getAuthenticatorUri(secretFromMaster);
      expect(uri).toContain('otpauth://totp');
      expect(uri).toContain('getanthill:me');
      expect(uri).toContain(secretFromMaster);
    });
  });
});
