import type { AuthGetTokensOptions, Tokens } from '../typings';

import * as crypto from './crypto';

const DEFAULT_TOKENS: string[] = ['auth', 'encryption'];

export function getTokens(
  email: string,
  password: string,
  options: AuthGetTokensOptions = {},
): Tokens {
  const result: Tokens = {
    auth: null,
    encryption: null,
  };

  const emailBits = crypto.kwe('quickStretch', email, options.namespace);
  const passwordBits = crypto.stringToBits(password);

  const derivedToken = crypto.pbkdf2(passwordBits, emailBits);
  const additionalTokens = options.tokens || [];
  for (const token of [...additionalTokens, ...DEFAULT_TOKENS]) {
    result[token] = crypto.hexFromBits(
      crypto.hkdf(derivedToken, crypto.kw(token)),
    );
  }

  return result;
}
