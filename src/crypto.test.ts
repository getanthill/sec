import * as sjcl from 'sjcl-including-ecc';
import * as crypto from './crypto';

describe('crypto', () => {
  describe('#kw', () => {
    it('must wrap a key with a name', () => {
      const wrapped = crypto.kw('hello');
      expect(wrapped).toEqual([
        1734702177, 1853122665, 1819029091, 1869426547, 1700999030, 825190501,
        26390098112256,
      ]);
      expect(sjcl.codec.utf8String.fromBits(wrapped)).toEqual(
        'getanthill.com/sec/v1/hello',
      );
    });
  });

  describe('#kwe', () => {
    it('must wrap a key with a name and an email', () => {
      const wrapped = crypto.kwe('john', 'john@doe.com');
      expect(wrapped).toEqual([
        1734702177, 1853122665, 1819029091, 1869426547, 1700999030, 825191023,
        1752054378, 1869114944, 1685021998, 26389947313408,
      ]);
      expect(sjcl.codec.utf8String.fromBits(wrapped)).toEqual(
        'getanthill.com/sec/v1/john:john@doe.com',
      );
    });
  });

  describe('#hkdf', () => {
    it('generates a valid key derivation of length 8', () => {
      const salt: number[] = sjcl.codec.hex.toBits('00');
      const len: number = 32;
      const key = crypto.hkdf('ikm', 'info', salt, len);

      expect(key.length).toEqual(8); // = 32 / 32 * 8
      expect(key).toEqual([
        329423567, -1264383065, -229071444, -229129894, -651605069, -209131660,
        -307169800, 12742773,
      ]);
    });

    it('generates a valid key derivation of length 16', () => {
      const salt: number[] = sjcl.codec.hex.toBits('00');
      const len: number = 64;
      const key = crypto.hkdf('ikm', 'info', salt, len);

      expect(key.length).toEqual(16); // = 64 / 32 * 8
    });

    it('generates 2 identical derived keys from the same inputs', () => {
      const salt: number[] = sjcl.codec.hex.toBits('00');
      const len: number = 64;
      const key1 = crypto.hkdf('ikm', 'info', salt, len);
      const key2 = crypto.hkdf('ikm', 'info', salt, len);

      expect(key1).toEqual(key2);
    });
  });

  describe('#pbkdf2', () => {
    it('generates a valid PKBDF2 derivation key', () => {
      const key = crypto.pbkdf2('password', 'john@doe.com');

      expect(key).toEqual([
        784158078, 1001122344, 1378421971, -466901223, -893570271, 1626693138,
        1116103054, 1451301888,
      ]);
    });

    it('generates twice the same key with identical inputs', () => {
      const key1 = crypto.pbkdf2('password', 'john@doe.com');
      const key2 = crypto.pbkdf2('password', 'john@doe.com');

      expect(key1).toEqual(key2);
    });
  });

  describe('#generateKeys', () => {
    it('returns one public key and one private key', () => {
      expect(Object.keys(crypto.generateKeys())).toEqual(['public', 'private']);
    });

    it('returns 2 keys we can use to encrypt and decrypt', () => {
      const keys = crypto.generateKeys();

      const clear = 'Hello World';

      expect(
        crypto.decrypt(keys.private, crypto.encrypt(keys.public, clear)),
      ).toEqual(clear);
    });
  });

  describe('#encryptJSON / #decryptJSON', () => {
    it('encrypts content', () => {
      const keys = crypto.generateKeys();

      expect(crypto.encryptJSON(keys.public, { a: 1 })).not.toEqual({ a: 1 });
    });

    it('allows to decrypt content with public and private keys', () => {
      const keys = crypto.generateKeys();

      expect(
        crypto.decryptJSON(
          keys.private,
          crypto.encryptJSON(keys.public, { a: 1 }),
        ),
      ).toEqual({ a: 1 });
    });

    it('allows to decrypt content with symetric key', () => {
      const key = 'Hello World';

      expect(
        crypto.decryptJSON(key, crypto.encryptJSON(key, { a: 1 })),
      ).toEqual({ a: 1 });
    });
  });
});
