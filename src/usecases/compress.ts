/**
 * @credits https://evanhahn.com/javascript-compression-streams-api-with-strings/
 */
import { Buffer } from 'buffer';

async function getValueFromReader(
  reader: ReadableStreamDefaultReader,
): Promise<any> {
  const chunks: Uint8Array[] = [];

  let res;
  do {
    res = await reader.read();

    res.value && chunks.push(res.value);
  } while (res.done === false);

  return concatUint8Arrays(chunks);
}

/**
 * Convert a string to its UTF-8 bytes and compress it.
 *
 * @param {string} str
 * @returns {Promise<Uint8Array>}
 */
async function compress(str: BlobPart) {
  // Convert the string to a byte stream.
  const stream = new Blob([str]).stream();

  // Create a compressed stream.
  const compressedStream = stream.pipeThrough(new CompressionStream('gzip'));

  const reader = compressedStream.getReader();

  return getValueFromReader(reader);
}

/**
 * Decompress bytes into a UTF-8 string.
 *
 * @param {Uint8Array} compressedBytes
 * @returns {Promise<string>}
 */
async function decompress(compressedBytes: BlobPart) {
  // Convert the bytes to a stream.
  const stream = new Blob([compressedBytes]).stream();

  // Create a decompressed stream.
  const decompressedStream = stream.pipeThrough(
    new DecompressionStream('gzip'),
  );

  const reader = decompressedStream.getReader();

  const stringBytes = await getValueFromReader(reader);

  // Convert the bytes to a string.
  return new TextDecoder().decode(stringBytes);
}

/**
 * Combine multiple Uint8Arrays into one.
 *
 * @param {ReadonlyArray<Uint8Array>} uint8arrays
 * @returns {Promise<Uint8Array>}
 */
async function concatUint8Arrays(uint8arrays: BlobPart[]): Promise<Uint8Array> {
  const blob = new Blob(uint8arrays);
  const buffer = await blob.arrayBuffer();
  return new Uint8Array(buffer);
}

export async function deflateData(data: any): Promise<any> {
  const d = await compress(JSON.stringify(data));

  return Buffer.from(d.buffer).toString('base64');
}

export async function unzipData(data: string): Promise<any> {
  const d = await decompress(Buffer.from(data, 'base64'));

  return JSON.parse(d.toString());
}
