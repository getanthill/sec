import * as crypto from '../crypto';
import * as compress from './compress';
import { encryptMessage, decryptMessage } from './crypto';

import { getTokens as _getTokens } from '../auth';

export async function encrypt(token: string, data: any): Promise<string> {
  const deflatedData = await compress.deflateData(data);

  return crypto.encrypt(token, deflatedData);
}

export async function decrypt(token: string, data: string): Promise<any> {
  const content = compress.unzipData(crypto.decrypt(token, data));

  return content;
}

export async function encryptWithPublicKey(
  publicKey: string,
  data: any,
): Promise<string> {
  const { encryption: encryptionKey } = await _getTokens(
    publicKey,
    `${Date.now()}`,
  );

  const encryptedData = await encrypt(encryptionKey, data);

  const encryptedEncryptionKey = await encryptMessage(publicKey, encryptionKey);

  return `${encryptedEncryptionKey}::${encryptedData}`;
}

export async function decryptWithPrivateKey(
  privateKey: string,
  data: string,
): Promise<any> {
  const [encryptedEncryptionKey, encryptedData] = data.split('::', 2);

  const encryptionKey = await decryptMessage(
    privateKey,
    encryptedEncryptionKey,
  );

  return decrypt(encryptionKey, encryptedData);
}
