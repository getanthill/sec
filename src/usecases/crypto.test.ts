/**
 * @jest-environment jsdom
 */

import * as crypto from './crypto';

import setup from '../../test/setup';

describe('crypto', () => {
  beforeAll(() => {
    setup.init();
  });
  describe('#getTokens', () => {
    it('produces tokens for a couple of email and password', async () => {
      const tokens = await crypto.getTokens('alice@doe.org', 'xxx');

      expect(tokens).toEqual({
        auth: '501e8eaa456098ba6e46ad95ec8eb49245112d1289ee0eb8ad9bf3609b20e15f',
        encryption:
          'd9a83fb4eb9df9dcf64cea0d4b10fb131cbc17755dd46f8afdc2170c7faef37d',
        storage:
          '7211e75c2ce79cedcf45fcd852f32636cf046be3d30767f864b3272987ac89a0',
      });
    });

    it('produces tokens with additional ones if requested', async () => {
      const tokens = await crypto.getTokens('alice@doe.org', 'xxx', ['share']);

      expect(tokens).toEqual({
        auth: '501e8eaa456098ba6e46ad95ec8eb49245112d1289ee0eb8ad9bf3609b20e15f',
        encryption:
          'd9a83fb4eb9df9dcf64cea0d4b10fb131cbc17755dd46f8afdc2170c7faef37d',
        storage:
          '7211e75c2ce79cedcf45fcd852f32636cf046be3d30767f864b3272987ac89a0',
        share:
          '799428fe707ce06cb52005a35d8617eb94a1fc37e5644378d3b315ddae608302',
      });
    });

    it('produces different tokens based on the namespace', async () => {
      const tokens = await crypto.getTokens(
        'alice@doe.org',
        'xxx',
        [],
        'namespace',
      );

      expect(tokens).toEqual({
        auth: '43a4b5c5bd764269e055934f6606a07ae4fabae95225c438ceea17f19b21850f',
        encryption:
          'd9ae938663231aca576c5a7daba8f2b59d7e557a10bac282b5b0ad234e83997c',
        storage:
          '2c330f20cea3cf2988f0dae8c44404198d6b9240915c80e07947f85ed5db32fa',
      });
    });
  });

  describe('#generateKeyPair', () => {
    it('generates public and private key pair', async () => {
      const keyPair = await crypto.generateKeyPair();

      expect(keyPair).toHaveProperty('privateKey');
      expect(keyPair).toHaveProperty('publicKey');
    });

    it('produces a public key on the PEM format', async () => {
      const keyPair = await crypto.generateKeyPair();

      expect(keyPair.publicKey).toContain('BEGIN PUBLIC KEY');
      expect(keyPair.publicKey).toContain('END PUBLIC KEY');
    });

    it('produces a private key on the PEM format', async () => {
      const keyPair = await crypto.generateKeyPair();

      expect(keyPair.privateKey).toContain('BEGIN PRIVATE KEY');
      expect(keyPair.privateKey).toContain('END PRIVATE KEY');
    });
  });

  describe('#encryptMessage / decryptMessage', () => {
    let keyPair;

    beforeEach(async () => {
      keyPair = await crypto.generateKeyPair();
    });

    it('allows to encrypt a text message', async () => {
      // Given
      const message = 'Hello world!';

      // When
      const encryptedMessage = await crypto.encryptMessage(
        keyPair.publicKey,
        message,
      );

      // Then
      expect(encryptedMessage).not.toEqual(message);

      const decryptedMessage = await crypto.decryptMessage(
        keyPair.privateKey,
        encryptedMessage,
      );
      expect(message).toEqual(decryptedMessage);
    });
  });
});
