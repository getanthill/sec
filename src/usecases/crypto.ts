import { Buffer } from 'buffer';
import { getTokens as _getTokens } from '../auth';

const KEY_PAIR_OPTIONS = {
  name: 'RSA-OAEP',
  modulusLength: 4096,
  publicExponent: new Uint8Array([1, 0, 1]),
  hash: 'SHA-256',
};
const PRIVATE_KEY_EXPORT_FORMAT = 'pkcs8';
const PUBLIC_KEY_EXPORT_FORMAT = 'spki';

export function getTokens(
  email: string,
  password: string,
  additionalTokenIds: string[] = [],
  namespace?: string,
): Record<string, string> {
  const tokens = _getTokens(email, password, {
    namespace,
    tokens: ['storage', ...additionalTokenIds],
  });

  return tokens;
}

/*
Export the given key and write it into the "exported-key" space.
*/
async function exportPrivateCryptoKey(key: CryptoKey) {
  const exported = await crypto.subtle.exportKey(
    PRIVATE_KEY_EXPORT_FORMAT,
    key,
  );
  const exportedAsString = ab2str(exported);
  const exportedAsBase64 = btoa(exportedAsString);
  const pemExported = `-----BEGIN PRIVATE KEY-----\n${exportedAsBase64}\n-----END PRIVATE KEY-----`;

  return pemExported;
}

/*
Export the given key and write it into the "exported-key" space.
*/
async function exportPublicCryptoKey(key: CryptoKey) {
  const exported = await crypto.subtle.exportKey('spki', key);
  const exportedAsString = ab2str(exported);
  const exportedAsBase64 = btoa(exportedAsString);
  const pemExported = `-----BEGIN PUBLIC KEY-----\n${exportedAsBase64}\n-----END PUBLIC KEY-----`;

  return pemExported;
}

async function exportKeys(
  key: CryptoKeyPair,
): Promise<{ publicKey: string; privateKey: string }> {
  const publicKey = await exportPublicCryptoKey(key.publicKey);

  const privateKey = await exportPrivateCryptoKey(key.privateKey);

  return {
    publicKey,
    privateKey,
  };
}

export async function generateKeyPair(): Promise<{
  privateKey: string;
  publicKey: string;
}> {
  let keyPair = await crypto.subtle.generateKey(KEY_PAIR_OPTIONS, true, [
    'encrypt',
    'decrypt',
  ]);

  const keys = exportKeys(keyPair);

  return keys;
}

async function importPublicKey(pemKey: string): Promise<CryptoKey> {
  const pemHeader = '-----BEGIN PUBLIC KEY-----';
  const pemFooter = '-----END PUBLIC KEY-----';
  const pemContents = pemKey.substring(
    pemHeader.length,
    pemKey.length - pemFooter.length - 1,
  );

  // base64 decode the string to get the binary data
  const binaryDerString = atob(pemContents);
  // convert from a binary string to an ArrayBuffer
  const binaryDer = str2ab(binaryDerString);

  return crypto.subtle.importKey(
    PUBLIC_KEY_EXPORT_FORMAT,
    binaryDer,
    KEY_PAIR_OPTIONS,
    true,
    ['encrypt'],
  );
}

async function importPrivateKey(pemKey: string): Promise<CryptoKey> {
  const pemHeader = '-----BEGIN PRIVATE KEY-----';
  const pemFooter = '-----END PRIVATE KEY-----';
  const pemContents = pemKey.substring(
    pemHeader.length,
    pemKey.length - pemFooter.length - 1,
  );

  // base64 decode the string to get the binary data
  const binaryDerString = atob(pemContents);
  // convert from a binary string to an ArrayBuffer
  const binaryDer = str2ab(binaryDerString);

  return crypto.subtle.importKey(
    PRIVATE_KEY_EXPORT_FORMAT,
    binaryDer,
    KEY_PAIR_OPTIONS,
    true,
    ['decrypt'],
  );
}

function getMessageEncoding(str: string) {
  let enc = new TextEncoder();
  return enc.encode(str);
}

function getMessageDecoding(input: AllowSharedBufferSource | undefined) {
  let dec = new TextDecoder();
  return dec.decode(input);
}

export async function encryptMessage(
  publicKey: string,
  message: string,
): Promise<string> {
  const key = await importPublicKey(publicKey);
  let encoded = getMessageEncoding(message);

  const buffer = await crypto.subtle.encrypt(KEY_PAIR_OPTIONS, key, encoded);

  return Buffer.from(buffer).toString('base64');
}

export async function decryptMessage(
  privateKey: string,
  encryptedMessageBase64: string,
) {
  const key = await importPrivateKey(privateKey);

  const encryptedMessage = Buffer.from(encryptedMessageBase64, 'base64');
  const decrypted = await crypto.subtle.decrypt(
    KEY_PAIR_OPTIONS,
    key,
    encryptedMessage,
  );

  return getMessageDecoding(decrypted);
}

// ----------------------------------------------

/*
Convert an ArrayBuffer into a string
from https://developer.chrome.com/blog/how-to-convert-arraybuffer-to-and-from-string/
*/
function ab2str(buf) {
  return String.fromCharCode.apply(null, new Uint8Array(buf));
}

/*
Convert a string into an ArrayBuffer
from https://developers.google.com/web/updates/2012/06/How-to-convert-ArrayBuffer-to-and-from-String
*/
function str2ab(str) {
  const buf = new ArrayBuffer(str.length);
  const bufView = new Uint8Array(buf);
  for (let i = 0, strLen = str.length; i < strLen; i++) {
    bufView[i] = str.charCodeAt(i);
  }
  return buf;
}
