import * as compress from './compress';
import * as crypto from './crypto';

export async function send(key: string, data: any): Promise<string> {
  const deflatedData = await compress.deflateData(data);

  return crypto.encryptMessage(key, deflatedData);
}

export async function read(key: string, data: string): Promise<any> {
  const content = await crypto.decryptMessage(key, data);

  return compress.unzipData(content);
}
