/**
 * @jest-environment jsdom
 */

import * as accounts from './accounts';
import * as messages from './messages';
import * as data from './data';

import setup from '../../test/setup';

describe('accounts', () => {
  beforeAll(() => {
    setup.init();
  });
  describe('#register', () => {
    it('produces tokens for an account', async () => {
      const { tokens, account } = await accounts.register(
        'alice@doe.org',
        'xxx',
      );

      expect(tokens).toEqual({
        auth: '501e8eaa456098ba6e46ad95ec8eb49245112d1289ee0eb8ad9bf3609b20e15f',
        encryption:
          'd9a83fb4eb9df9dcf64cea0d4b10fb131cbc17755dd46f8afdc2170c7faef37d',
        storage:
          '7211e75c2ce79cedcf45fcd852f32636cf046be3d30767f864b3272987ac89a0',
      });
    });

    it('produces a public key for the account', async () => {
      const { account } = await accounts.register('alice@doe.org', 'xxx');

      expect(account).toHaveProperty('public_key');
    });

    it('stores account data safely', async () => {
      const { account } = await accounts.register('alice@doe.org', 'xxx');

      expect(account).toHaveProperty('data');
      const parsedData = JSON.parse(account.data);

      expect(parsedData).toMatchObject({
        mode: 'ccm',
        cipher: 'aes',
      });
    });

    it('allows to register an account with valid public and private keys', async () => {
      const { tokens, account } = await accounts.register(
        'alice@doe.org',
        'xxx',
      );

      const publicKey = account.public_key!;
      const { private_key: privateKey } = await data.decrypt(
        tokens.encryption,
        account.data,
      );

      const encryptedMessage = await messages.send(publicKey, 'Hello World');
      const message = await messages.read(privateKey, encryptedMessage);

      expect(message).toEqual('Hello World');
    }, 10000);
  });
});
