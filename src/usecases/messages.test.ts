/**
 * @jest-environment jsdom
 */

import * as crypto from './crypto';
import * as messages from './messages';

import setup from '../../test/setup';

describe('messages', () => {
  let keyPair;

  beforeAll(() => {
    setup.init();
  });

  beforeEach(async () => {
    keyPair = await crypto.generateKeyPair();
  });

  describe('#send / read', () => {
    it('allows to send text messages', async () => {
      // Given
      const content = 'Hello World!';

      // When
      const sentContent = await messages.send(keyPair.publicKey, content);

      // Expect
      expect(sentContent).not.toEqual(content);

      const receivedContent = await messages.read(
        keyPair.privateKey,
        sentContent,
      );
      expect(receivedContent).toEqual(content);
    });

    it('allows to send number', async () => {
      // Given
      const content = 12.34;

      // When
      const sentContent = await messages.send(keyPair.publicKey, content);

      // Expect
      expect(sentContent).not.toEqual(content);

      const receivedContent = await messages.read(
        keyPair.privateKey,
        sentContent,
      );
      expect(receivedContent).toEqual(content);
    });

    it('allows to send boolean', async () => {
      // Given
      const content = true;

      // When
      const sentContent = await messages.send(keyPair.publicKey, content);

      // Expect
      expect(sentContent).not.toEqual(content);

      const receivedContent = await messages.read(
        keyPair.privateKey,
        sentContent,
      );
      expect(receivedContent).toEqual(content);
    });

    it('allows to send object', async () => {
      // Given
      const content = {
        hello: {
          world: true,
        },
      };

      // When
      const sentContent = await messages.send(keyPair.publicKey, content);

      // Expect
      expect(sentContent).not.toEqual(content);

      const receivedContent = await messages.read(
        keyPair.privateKey,
        sentContent,
      );
      expect(receivedContent).toEqual(content);
    });
  });
});
