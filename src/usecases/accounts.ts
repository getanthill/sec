import * as crypto from './crypto';
import * as data from './data';

export interface AccountCreated {
  /**
   * Encrypted data
   */
  data: string;
  /**
   * Public key
   */
  public_key?: string;
}

export async function register(
  email: string,
  password: string,
  additionalTokenIds?: string[],
  namespace?: string,
): Promise<{ tokens: Record<string, string>; account: AccountCreated }> {
  const tokens = crypto.getTokens(
    email,
    password,
    additionalTokenIds,
    namespace,
  );

  const { privateKey, publicKey } = await crypto.generateKeyPair();

  const accountData = await data.encrypt(tokens.encryption, {
    email,
    private_key: privateKey,
  });

  const account: AccountCreated = {
    public_key: publicKey,
    data: accountData,
  };

  return { tokens, account };
}
