/**
 * @jest-environment jsdom
 */

import * as accounts from './accounts';
import * as data from './data';
import * as crypto from './crypto';

import setup from '../../test/setup';

describe('data', () => {
  let registration;

  beforeAll(() => {
    setup.init();
  });

  beforeEach(async () => {
    registration = await accounts.register(
      `john+${Date.now}@doe.org`,
      `${Date.now}`,
    );
  });

  describe('#encrypt / decrypt', () => {
    it('allows to encrypt text', async () => {
      // Given
      const content = 'Hello World!';

      // When
      const encryptedContent = await data.encrypt(
        registration.tokens.encryption,
        content,
      );

      // Then
      expect(encryptedContent).not.toEqual(content);

      const decryptedContent = await data.decrypt(
        registration.tokens.encryption,
        encryptedContent,
      );
      expect(decryptedContent).toEqual(content);
    });

    it('allows to encrypt number', async () => {
      // Given
      const content = 12.34;

      // When
      const encryptedContent = await data.encrypt(
        registration.tokens.encryption,
        content,
      );

      // Then
      expect(encryptedContent).not.toEqual(content);

      const decryptedContent = await data.decrypt(
        registration.tokens.encryption,
        encryptedContent,
      );
      expect(decryptedContent).toEqual(content);
    });

    it('allows to encrypt boolean', async () => {
      // Given
      const content = true;

      // When
      const encryptedContent = await data.encrypt(
        registration.tokens.encryption,
        content,
      );

      // Then
      expect(encryptedContent).not.toEqual(content);

      const decryptedContent = await data.decrypt(
        registration.tokens.encryption,
        encryptedContent,
      );
      expect(decryptedContent).toEqual(content);
    });

    it('allows to encrypt dates but returns ISOString', async () => {
      // Given
      const content = new Date('2023-01-01T00:00:00.000Z');

      // When
      const encryptedContent = await data.encrypt(
        registration.tokens.encryption,
        content,
      );

      // Then
      expect(encryptedContent).not.toEqual(content);

      const decryptedContent = await data.decrypt(
        registration.tokens.encryption,
        encryptedContent,
      );
      expect(decryptedContent).toEqual(content.toISOString());
    });

    it('allows to encrypt object', async () => {
      // Given
      const content = {
        hello: {
          world: true,
        },
      };

      // When
      const encryptedContent = await data.encrypt(
        registration.tokens.encryption,
        content,
      );

      // Then
      expect(encryptedContent).not.toEqual(content);

      const decryptedContent = await data.decrypt(
        registration.tokens.encryption,
        encryptedContent,
      );
      expect(decryptedContent).toEqual(content);
    });

    it('allows to encrypt big files', async () => {
      // Given
      const keyPair = await crypto.generateKeyPair();
      const content = Array(2048 * 2).fill('1');

      // When
      const encryptedContent = await data.encryptWithPublicKey(
        keyPair.publicKey,
        content,
      );

      // Then
      expect(encryptedContent).not.toEqual(content);

      const decryptedContent = await data.decryptWithPrivateKey(
        keyPair.privateKey,
        encryptedContent,
      );
      expect(decryptedContent).toEqual(content);
    });
  });
});
