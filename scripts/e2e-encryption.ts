import * as sjcl from 'sjcl-including-ecc';

import {
  getTokens,
  generateKeys,
  encrypt,
  encryptJSON,
  decrypt,
  decryptJSON,
} from '../src';

async function main() {
  console.log('[e2e] Starting...');

  const alice: any = {};
  const bernard: any = {};

  console.log('[e2e] Getting tokens for Alice and Bernard...');
  alice.tokens = getTokens('alice@doe.com', 'AlicePassword', {
    tokens: ['keys'],
  });
  bernard.tokens = getTokens('bernard@doe.com', 'BernardPassword', {
    tokens: ['keys'],
  });

  console.log('[e2e] Getting public/private keys for Alice and Bernard...');

  alice.keys = generateKeys();
  bernard.keys = generateKeys();

  console.log('[e2e] Encrypt keys with personal tokens');
  alice.encrypted_keys = encryptJSON(alice.tokens.keys, alice.keys);
  bernard.encrypted_keys = encryptJSON(bernard.tokens.keys, bernard.keys);

  console.log(
    `[e2e] Initializing public/private keys for the
  newly created conversation...`,
  );

  console.log('[e2e] Bernard is creating a new conversation');
  const conversation = {
    keys: generateKeys(),
  };

  console.log(`[e2e] Encrypt conversation keys for Bernard
  with his public key`);
  const bernardMembership = {
    encrypted_keys: encryptJSON(bernard.keys.public, conversation.keys),
  };

  console.log(
    `[e2e] Bernard is inviting Alice giving her the keys,
  encrypted with her public key`,
  );
  const aliceMembership = {
    encrypted_keys: encryptJSON(alice.keys.public, conversation.keys),
  };

  console.log('[e2e] Decrypt conversation keys with Alice private key');
  const decryptedMembership = {
    keys: decryptJSON(alice.keys.private, aliceMembership.encrypted_keys),
  };

  var plain = 'hello world!';
  console.log(
    `[e2e] Bernard is encrypting a message with the
  conversation public key`,
  );
  var cipher = encrypt(decryptedMembership.keys.public, plain);

  console.log(
    `[e2e] Alice is decrypting the message with the
  shared conversation private key`,
  );
  var result = decrypt(decryptedMembership.keys.private, cipher);

  console.log('[e2e] Do we have the exact same text?', plain === result);
}

main()
  .then(() => {
    console.log('Script ended successfully');
  })
  .catch((err) => {
    console.error(err);

    process.exit(1);
  });
