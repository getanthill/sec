module.exports = {
  collectCoverageFrom: ['src/**/*.{js,jsx,ts}', '!**/node_modules/**'],
  testTimeout: parseInt(process.env.JEST_TEST_TIMEOUT || '10000', 10),
  coverageThreshold: {
    global: {
      branches: 100,
      functions: 100,
      lines: 100,
      statements: 100,
    },
  },
  globalSetup: '<rootDir>/test/setup.ts',
  transform: {
    '^.+\\.(ts|js)$': 'babel-jest',
  },
};
